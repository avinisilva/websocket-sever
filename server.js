var fs = require('fs');
var http = require('http').createServer(handler)
var io = require('socket.io')(http)
var Redis = require('ioredis')
var redis = new Redis(6379, '127.0.0.1')

redis.psubscribe('*', function() {

    redis.on('pmessage', function(pattern, channel, message) {
        io.emit(channel, message)
    })

})

http.listen(3000, function(){
    console.log('Listening on Port 3000')
})

function handler (req, res) {
  fs.readFile(__dirname + '/index.html',
  function (err, data) {
    if (err) {
      res.writeHead(500)
      return res.end('Error loading index.html')
    }

    res.writeHead(200)
    res.end(data)
  })
}

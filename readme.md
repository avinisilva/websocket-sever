# Socket.io com Redis

![image](https://cdn-images-1.medium.com/max/800/1*PE49ijmPKhm23Mo2_iAjwg.png)

Executando o projeto:

Instale o docker e o docker-compose pois nesse exemplo rodo o Redis em um container docker.

- Clone o projeto, entre na pasta e execute `yarn install` ou `npm i`.

- Execute o comando `sudo docker-compose up -d`.

- Agora execute `yarn run dev` caso use npm `npm run dev`.

- Entre em http://localhost:3000.

- Abra outro terminal e execute `sudo docker exec -it redis-server bash` depois de entrar no container execute `redis-cli`.

- No redis CLI execute `publish products "product created"`.

- Verifique se a messagem foi adicionada no HTML.

[Link para o artigo no Medium](https://medium.com/@avinicius.adorno/a-necessidade-de-criar-um-servidor-web-socket-63a7c56a1698#.7d6rhl19g)
